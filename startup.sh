#!/bin/bash

tmux new-session -d -s 'build' 'bash'\; \
send-keys 'source /root/.bash_profile' C-m \; \
send-keys 'cd /root/wowrewards/backend' C-m \; \
send-keys 'go build' C-m \; \
send-keys './backend' C-m \; \
split-window -v 'bash'\; \
send-keys 'source /root/.bash_profile' C-m \; \
send-keys 'cd /root/wowrewards/frontend/' C-m \; \
send-keys 'npm update' C-m \; \
send-keys 'npm run build ' C-m \; \
send-keys 'serve -s build' C-m \;