import React, { Component } from 'react';

class Accounts extends Component {
    constructor() {
        super();
        this.state = {
            accounts: [],
        };
    }

    componentWillMount() {
        fetch('https://rewards.whispersofwood.com/accounts')
            .then(results => {
                //console.log(results);
                return results.json();
            }).then(data => {
                //console.log(data);
                let accounts = data.map((account) => {
                    return(
                        <div align='centre' key={account.accountid}>
                            <ul><strong>Account:</strong> {account.accountid} - {account.rewardpoints}</ul>
                        </div>
                    )
                });
            this.setState({accounts: accounts});
                //console.log("state", this.state.accounts)
        })
    }

    render() {
        return (
            <div className="Accounts">
                {this.state.accounts}
            </div>
        )
    }
}

export default Accounts;
