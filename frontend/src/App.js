import React, { Component } from 'react';
import logo from './logo.jpg';
import './App.css';
import Accounts from './Components/Accounts';

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Whispers of Wood Rewards</h1>
        </header>
        <p className="App-intro">
          Welcome to the Whispers of Wood Admin Interface!
        </p>
          <Accounts />
      </div>
    );
  }
}

export default App;
