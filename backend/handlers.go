package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

//Index does index things
func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "These are not the droids you're looking for...")
}

//AccountIndex does AccountIndex things
func AccountIndex(w http.ResponseWriter, r *http.Request) {
	//vars := mux.Vars(r)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	if err := json.NewEncoder(w).Encode(MongoFindAll(1000, "mongodb://db:27017/wowrewards")); err != nil {
		log.Println(err)
	}
}

//AccountShow does AccountShow things
func AccountShow(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	acctID := vars["acctID"]

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)


	if err := json.NewEncoder(w).Encode(MongoFind(acctID, "mongodb://db:27017/wowrewards")); err != nil {
		log.Print(err)
	}
}

//AccountCreate - creates accounts ;)
func AccountCreate(w http.ResponseWriter, r *http.Request) {
	var account Account
	//vars := mux.Vars(r)
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))

	if err != nil {
		panic(err)
	}

	if err := r.Body.Close(); err != nil {
		panic(err)
	}

	if err := json.Unmarshal(body, &account); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // un-processable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
	}

	result := MongoCreate(account, "mongodb://db:27017/wowrewards") //write db insert method before completing this
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(result); err != nil {
		log.Print(err)
	}

}

//AccountUpdate - updates account details
func AccountUpdate (w http.ResponseWriter, r *http.Request) {
	var account Account
	//vars := mux.Vars(r)
	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))

	if err != nil {
		panic(err)
	}

	if err := r.Body.Close(); err != nil {
		panic(err)
	}

	if err := json.Unmarshal(body, &account); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // un-processable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
	}

	result := MongoUpdate(account, "mongodb://db:27017/wowrewards")
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(result); err != nil {
		log.Print(err)
	}
}

//API Auth Function
func Auth(fn http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		user, pass, _ := r.BasicAuth()
		if (user != "test") && (pass != "test") {
		   http.Error(w, "Unauthorized.", http.StatusUnauthorized)
		   return
		}
		fn(w, r)
	}
}

//future replacement of user/pass check
//if !check(user, pass) {
//	http.Error(w, "Unauthorized.", http.StatusUnauthorized)
//	return
// }