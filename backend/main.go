package main

import (
	"log"
	"net/http"
	"crypto/tls"

	"golang.org/x/crypto/acme/autocert"
	)

func main() {
	router := NewRouter()

	certMgr := &autocert.Manager{
		Prompt:		autocert.AcceptTOS,
		Cache:		autocert.DirCache("/certs"),
		HostPolicy:	autocert.HostWhitelist("rewards.whispersofwood.com"),
		Email:		"josh@joshjacobs.net",
	}

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
        w.Write([]byte("Hello world"))
    })

	server := &http.Server{
		Addr:		":https",
		TLSConfig:	&tls.Config{
			GetCertificate: certMgr.GetCertificate,
		},
		Handler:	router,
	}

	//go http.ListenAndServe(":80", certMgr.HTTPHandler(nil))
	log.Fatal(server.ListenAndServeTLS("", ""))
}
