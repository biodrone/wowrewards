package main

import (
	"net/http"
)

//Route -
type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

//Routes -
type Routes []Route

var routes = Routes{
	Route{
		"Index",
		"GET",
		"/",
		Auth(Index),
	},
	Route{
		"AccountIndex",
		"GET",
		"/accounts",
		Auth(AccountIndex),
	},
	Route{
		"AccountShow",
		"GET",
		"/account/{acctID}",
		Auth(AccountShow),
	},
	Route{
		"AccountUpdate",
		"POST",
		"/account",
		Auth(AccountUpdate),
	},
	Route{
		"AccountCreate",
		"POST",
		"/account/new",
		Auth(AccountCreate),
	},
}
